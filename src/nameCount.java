
import java.util.*;

public class nameCount  {
	
	public static void main(String[]args) {
		
		HashMap<String,Integer> name = new HashMap<String,Integer>();
		
		getName(name);
		print(name);
		
	}
		private static void getName(Map<String,Integer> map) {
			while (true) {
				Scanner input = new Scanner(System.in);
				System.out.print("Enter Name: ");
				String name = input.nextLine();
				if(name.equals("")) break;
				
				Integer count = map.get(name);
				if(count == null) {
					count = new Integer(1);
				} else {
					count = new Integer(count + 1);
				}
				map.put(name, count);
			}
		}
		
		private static void print(Map<String, Integer> map) {
			Iterator<String> it = map.keySet().iterator();
			while (it.hasNext()) {
				String key = it.next();
				int count = map.get(key);
				System.out.println("Name: " + key + " has count " + count);
			}
		}
}

