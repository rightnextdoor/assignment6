import acm.program.*;
import acm.graphics.*;

public class ImageProcessing extends GraphicsProgram {
	
	public void run() {
		GImage image = new GImage("11.jpg");
		GImage flipImage = flipHorizontal(image);
		
		image.scale(0.8);
		add(image, 10, 50);
		
		flipImage.scale(0.8);
		add(flipImage, 600, 50);
		
	}
	
	private GImage flipHorizontal (GImage image) {
		
		int[][] array = image.getPixelArray();
		int width = array[0].length;
		int height = array.length;
		for (int row = 0; row < height; row++) {
			for (int j = 0; j < width / 2; j++) {
				int newWidth = width - j - 1;
				int temp = array[row][j];
				array[row][j] = array[row][newWidth];
				array[row][newWidth] = temp;
			}
		}
		return new GImage(array);
	}

}
